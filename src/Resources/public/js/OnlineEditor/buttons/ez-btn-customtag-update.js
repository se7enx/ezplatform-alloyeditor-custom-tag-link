import React from 'react';
import PropTypes from 'prop-types';
import EzBtnCustomTagUpdate
    from '../../../../../../../../ezsystems/ezplatform-richtext/src/bundle/Resources/public/js/OnlineEditor/buttons/ez-btn-customtag-update';

export default class ExtendedEzBtnCustomTagUpdate extends EzBtnCustomTagUpdate {
    constructor(props) {
        super(props);

        this.state.linkDetails = {};
    }

    /**
     * Renders the link.
     *
     * @method renderLink
     * @param {Object} config
     * @param {String} attrName
     * @return {Object} The rendered link.
     */
    renderLink(config, attrName) {
        const selectContentLabel = Translator.trans(
            /*@Desc("Select content")*/ 'custom_tag.link.select_content_btn.label',
            {},
            'alloy_editor'
        );
        if (this.state.values[attrName].value) {
            this.loadLinkContentInfo(attrName);
        }
        const linkDetails = this.state.linkDetails[attrName];

        return (
            <div className="attribute__wrapper">
                <label className="attribute__label form-control-label">{config.label}</label>
                <input
                    type="text"
                    defaultValue={config.defaultValue}
                    required={config.required}
                    className="attribute__input form-control"
                    value={this.state.values[attrName].value}
                    onChange={this.updateLinkValues.bind(this)}
                    data-attr-name={attrName}
                />
                <div className="ez-custom-tag__link-controls">
                    {linkDetails
                        ? <a href={linkDetails.href} target="_blank" className="ez-custom-tag--link"
                             title={linkDetails.title}>{linkDetails.title}</a>
                        : ''
                    }
                    <button
                        className="ez-btn-ae btn btn-secondary"
                        onClick={this.selectContent.bind(this, attrName)}
                    >
                        {selectContentLabel}
                    </button>
                </div>
            </div>
        );
    }

    updateLinkValues(event) {
        this.updateValues(event);
        this.loadLinkContentInfo(event.target.dataset.attrName);
    }

    loadLinkContentInfo(attrName) {
        const inputValue = this.state.values[attrName].value;
        const linkDetails = this.state.linkDetails[attrName] || {};

        let filter;
        if (inputValue.startsWith('ezlocation://') && inputValue.length > 13) {
            const locationId = parseInt(inputValue.substring(13));
            if (!locationId) {
                this.clearLinkDetails(attrName);
                return;
            }
            if (linkDetails.locationId === locationId) {
                return;
            }
            linkDetails.locationId = locationId;
            filter = { LocationIdCriterion: locationId };
        } else if (inputValue.startsWith('ezcontent://') && inputValue.length > 14) {
            const contentId = parseInt(inputValue.substring(14));
            if (!contentId) {
                this.clearLinkDetails(attrName);
                return;
            }
            if (linkDetails.contentId === contentId) {
                return;
            }
            linkDetails.contentId = contentId;
            filter = { ContentIdCriterion: contentId };
        } else {
            this.clearLinkDetails(attrName);
            return;
        }

        const token = document.querySelector('meta[name="CSRF-Token"]').content;
        const siteaccess = document.querySelector('meta[name="SiteAccess"]').content;

        const body = JSON.stringify({
            ViewInput: {
                identifier: `custom-tag-link-info-by-id-${linkDetails.contentId || ''}-${linkDetails.locationId || ''}`,
                public: false,
                LocationQuery: {
                    Criteria: {},
                    FacetBuilders: {},
                    SortClauses: { LocationDepth: 'ascending' },
                    Filter: filter,
                    limit: 1,
                    offset: 0,
                },
            },
        });
        const request = new Request('/api/ezp/v2/views', {
            method: 'POST',
            headers: {
                Accept: 'application/vnd.ez.api.View+json; version=1.1',
                'Content-Type': 'application/vnd.ez.api.ViewInput+json; version=1.1',
                'X-Requested-With': 'XMLHttpRequest',
                'X-Siteaccess': siteaccess,
                'X-CSRF-Token': token,
            },
            body,
            mode: 'same-origin',
            credentials: 'same-origin',
        });

        fetch(request)
            .then(window.eZ.helpers.request.getJsonFromResponse)
            .then((viewData) => {
                const resHits = viewData.View.Result.searchHits.searchHit;
                if (!resHits.length || !resHits[0].value) {
                    this.clearLinkDetails(attrName)
                    return;
                }

                this.setLinkDetails(attrName, resHits[0].value.Location);
            });
    }

    setLinkDetails(attrName, location) {
        const content = location.ContentInfo.Content;
        const linkDetails = Object.assign({}, this.state.linkDetails);
        linkDetails[attrName] = {
            title: content.TranslatedName || content.Name || '',
            href: Routing.generate('_ez_content_view', {
                contentId: content._id,
                locationId: location.id,
            }),
            contentId: content._id,
            locationId: location.id,
        };
        this.setState({ linkDetails });
    }

    clearLinkDetails(attrName) {
        if (this.state.linkDetails[attrName]) {
            const linkDetails = Object.assign({}, this.state.linkDetails);
            delete linkDetails[attrName];
            this.setState({ linkDetails });
        }
    }

    /**
     * Runs the Universal Discovery Widget so that the user can pick a Content.
     *
     * @method selectContent
     * @protected
     */
    selectContent(attrName) {
        const openUDW = () => {
            const config = JSON.parse(document.querySelector(`[data-udw-config-name="richtext_embed"]`).dataset.udwConfig);
            const title = Translator.trans(/*@Desc("Select content")*/ 'custom_tag.link.udw.title', {}, 'alloy_editor');
            const selectContent = eZ.richText.alloyEditor.callbacks.selectContent;
            const mergedConfig = Object.assign(
                {
                    onConfirm: this.udwOnConfirm.bind(this, attrName),
                    onCancel: this.udwOnCancel.bind(this),
                    title,
                    multiple: false,
                },
                config
            );

            if (typeof selectContent === 'function') {
                selectContent(mergedConfig);
            }
        };
        openUDW();

        this.disableUDWPropagation();
    }

    udwOnConfirm(attrName, items) {
        this.state.values[attrName].value = 'ezlocation://' + items[0].id;
        this.setLinkDetails(attrName, items[0]);

        this.enableUDWPropagation();
    }

    udwOnCancel() {
        ReactDOM.unmountComponentAtNode(document.querySelector('#react-udw'));
        this.enableUDWPropagation();
    }

    /**
     * Disable propagation to make sure attributes toolbar
     * not closed by alloyeditor outside click
     */
    disableUDWPropagation() {
        const container = document.querySelector('body');
        container.addEventListener('mousedown', this.doNotPropagate);
        container.addEventListener('keydown', this.doNotPropagate);
    }

    enableUDWPropagation() {
        const container = document.querySelector('body');
        container.removeEventListener('mousedown', this.doNotPropagate);
        container.removeEventListener('keydown', this.doNotPropagate);
    }

    doNotPropagate(event) {
        event.stopPropagation();
    }
}

const eZ = (window.eZ = window.eZ || {});

eZ.ezAlloyEditor = eZ.ezAlloyEditor || {};
eZ.ezAlloyEditor.ezBtnCustomTagUpdate = ExtendedEzBtnCustomTagUpdate;

EzBtnCustomTagUpdate.defaultProps = {
    command: 'ezcustomtag',
    modifiesSelection: true,
};

EzBtnCustomTagUpdate.propTypes = {
    editor: PropTypes.object.isRequired,
    label: PropTypes.string.isRequired,
    tabIndex: PropTypes.number.isRequired,
};
